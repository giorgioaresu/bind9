FROM alpine:latest

MAINTAINER Giorgio Aresu <giorgioaresu@gmail.com>

RUN apk --no-cache add bind drill

VOLUME ["/etc/bind"]
EXPOSE 53
EXPOSE 53/udp

HEALTHCHECK --interval=1m --timeout=10s \
  CMD drill www.google.com @localhost

CMD ["named", "-g", "-u", "named"]