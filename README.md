# Bind9
[![build status](https://gitlab.com/giorgioaresu/bind9/badges/master/build.svg)](https://gitlab.com/giorgioaresu/bind9/commits/master)
## Description

This docker image provides a [bind service](https://www.isc.org/downloads/bind/) based on [Alpine Linux](https://hub.docker.com/_/alpine/).

## Usage
Use this docker command to run the bind container.
```shell
docker run -d --name bind -p 53:53 \
-v /host/path/named.conf:/etc/bind/named.conf \
-v /host/path/example.com.db:/etc/bind/example.com.db \
giorgiaresu/bind9:latest
```
You can add directory or several files with the -v option. For a complete list of possible parameters, please refer to [docker documentation](https://docs.docker.com/engine/reference/commandline/run/)

The command will output the container ID, which you can use to check health status by issuing:
```shell
docker inspect --format "{{ .State.Health.Status }}" container_id
```

## Test
You can test the dns responses with `dig` or `nslookup` on the docker host.
```
dig webmail.example.com @localhost

nslookup webmail.example.com localhost
```